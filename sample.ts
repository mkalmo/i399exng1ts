import { Post } from './post';

class Dao {
    getPost() {
        let dataFromServer = '{ "title": "Post 1", "text": "text 1" }';

        return JSON.parse(dataFromServer);
    }

    getRealPost() {
        return new Post('Title 1', 'text 1');
    }

    getPostTyped(): Post {
        let dataFromServer = '{ "title": "Post 1", "text": "text 1" }';

        return <Post> JSON.parse(dataFromServer);
    }
}

let dao = new Dao();

console.log(dao.getRealPost());
