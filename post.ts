export class Post {
    constructor(public title: string, public text: string) {}

    getTitle() {
        return this.title;
    }

}
