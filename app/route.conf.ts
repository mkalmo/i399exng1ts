import { ILocationProvider } from 'angular';

routeConf.$inject = ['$routeProvider', '$locationProvider'];

function routeConf($routeProvider: any,
                   $locationProvider: any) {

    $routeProvider.when('/list', {
        templateUrl : 'app/list.html',
        controller : 'ListCtrl',
        controllerAs : 'vm'
    }).when('/new', {
        templateUrl : 'app/add.html',
        controller : 'AddCtrl',
        controllerAs : 'vm'
    }).otherwise({ redirectTo: '/list' });

    $locationProvider.hashPrefix('');
}

angular.module('app').config(routeConf);
