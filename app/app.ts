var app = angular.module('app', ['ngRoute']);

import './mem.data.srv';
import './add.ctrl';
import './list.ctrl';
import './route.conf';

angular.element(document)
    .ready(function () {
        angular.bootstrap(document, ['app']);
    });
