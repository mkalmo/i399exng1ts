import { Post } from './post.cls';
import { DataService } from './data.srv.i';
import { ILocationService } from 'angular';

function AddCtrl($location: any, dataService: any) {

    var vm = this;
    vm.title = '';
    vm.text = '';
    vm.addNew = addNew;

    function addNew() {
        var post = { title: vm.title, text: vm.text }; // use new Post(.. instead

        dataService.addPost(post)
            .then(function () {
                $location.path('/list');
            });
    }
}

AddCtrl.$inject = ['$location', 'dataService'];

angular.module('app').controller('AddCtrl', AddCtrl);
