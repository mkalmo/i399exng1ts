import { Post } from './post.cls';
import { DataService } from './data.srv.i';
import { IQService } from 'angular';

function MemDataService($q: any) {

    var vm = this;
    vm.addPost = addPost;
    vm.getPosts = getPosts;

    vm.posts = [
        new Post('Post 1', 'text 1'),
        new Post('Post 2', 'text 2')
    ];

    function addPost(post: Post) {
        vm.posts.push(post);

        return $q.resolve();
    }

    function getPosts() {
        return $q.resolve(vm.posts);
    }
}

MemDataService.$inject = ['$q'];

angular.module('app').service('dataService', MemDataService);
