import { Post } from './post.cls';
import { DataService } from './data.srv.i';
import { IHttpService } from 'angular';

export class RestDataService implements DataService {

    constructor(private $http: IHttpService) {}

    addPost(post: Post): Promise<void> {
        return this.$http.post('api/posts', post)
            .then(() => <void> null);
    }

    getPosts(): Promise<Post[]> {
        return this.$http.get('api/posts')
            .then(result => <Post[]> result.data);
    }
}

RestDataService.$inject = ['$http'];

angular.module('app').service('dataService', RestDataService);
