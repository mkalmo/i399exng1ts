import { Post } from './post.cls';
import { DataService } from "./data.srv.i";

function ListCtrl(dataService: any) {

    var vm = this;
    vm.posts = [];

    init();

    function init() {
        dataService.getPosts()
            .then(function (result : any) {
                vm.posts = result;
            });
    }
}

ListCtrl.$inject = ['dataService'];

angular.module('app').controller('ListCtrl', ListCtrl);
