import { Post } from './post.cls';

export interface DataService {

    addPost(post: Post): Promise<void>;

    getPosts(): Promise<Post[]>;
}
